import * as Firebase from "firebase";
import "firebase/firestore";

import FirebaseConfig from "../../constants/firebase.config";

const initialize = async () => {
  Firebase.initializeApp(FirebaseConfig);
  await Firebase.auth().signInAnonymously();
};

const isInitialized = () => Firebase.apps.length;

export const FirebaseClient = {
  connect: async () => {
    if (!isInitialized()) {
      await initialize();
    }
  },
  db: async () => {
    if (!isInitialized()) {
      await initialize();
    }

    return Firebase.firestore();
  },
};
