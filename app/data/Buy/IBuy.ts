export interface IBuy {
  ref: string;
  type: "Drop n Go",
  itemCount: number;
  orderTotal: number;
  status: string;
}
