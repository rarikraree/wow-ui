import { FirebaseClient } from "../common/FirebaseClient";
import { IBuy } from "./IBuy";

const Collection = async () => {
  const db = await FirebaseClient.db();

  return db.collection("Buy")
}

const MyCouponsData = {
  getAll: async () => {
    const collection = await Collection();
    const snapshot = await collection.get();
    const list = [];
    snapshot.forEach((doc) => list.push(doc.data()));

    return list;
  },
  setDropAndGo: async (ref: string) => {
    try {
      const collection = await Collection();
      const buy: IBuy = {
        ref,
        itemCount: 0,
        orderTotal: 0,
        status: "waiting",
        type: "Drop n Go",
      }
      await collection.doc(ref).set(buy);

      return {
        success: true,
      };
    } catch (error) {
      return {
        success: false,
        error,
      };
    }
  },
  delete: async (ref: string) => {
    try {
      const collection = await Collection();
      await collection.doc(ref).delete();

      return {
        success: true,
      };
    } catch (error) {
      return {
        success: false,
        error,
      };
    }
  },
};

export default MyCouponsData;
