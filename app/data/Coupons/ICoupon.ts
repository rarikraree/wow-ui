export interface ICoupon {
  _id: string;
  type: string;
  vender: string;
  title: string;
  description: string;
  product?: {
    id: string;
    basePrice: number;
    salePrice: number;
  };
  specID: string;
  created: string;
  expired: string;
}
