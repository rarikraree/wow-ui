import { FirebaseClient } from "../common/FirebaseClient";
import { ICoupon } from "./ICoupon";
import * as UUID from "uuid";

const Collection = async () => {
  const db = await FirebaseClient.db();

  return db.collection("MyCoupons")
}

const MyCouponsData = {
  getAll: async () => {
    const collection = await Collection();
    const snapshot = await collection.get();
    const list = [];
    snapshot.forEach((doc) => list.push(doc.data()));

    return list;
  },
  set: async (coupon: ICoupon) => {
    try {
      const collection = await Collection();
      await collection.doc(coupon._id).set(coupon);

      return {
        success: true,
      };
    } catch (error) {
      return {
        success: false,
        error,
      };
    }
  },
  delete: async (couponID: string) => {
    try {
      const collection = await Collection();
      await collection.doc(couponID).delete();

      return {
        success: true,
      };
    } catch (error) {
      return {
        success: false,
        error,
      };
    }
  },
};

export default MyCouponsData;
