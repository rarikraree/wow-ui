export interface ICouponSpec {
  _id: string;
  type: string;
  vender: string;
  title: string;
  description?: string;
  product?: {
    id: string;
  };
  price: {
    base: number;
    min: number;
    max: number;
  };
  lifesec: number;
}
