import * as UUID from "uuid";
import { FirebaseClient } from "../common/FirebaseClient";
import { ICoupon } from "./ICoupon";
import { ICouponSpec } from "./ICouponSpec";

const Collection = async () => {
  const db = await FirebaseClient.db();

  return db.collection("CouponSpec");
}

export const transform = (spec: ICouponSpec, type?: string): ICoupon => {
  const range = spec.price.max - spec.price.min;
  const hit = Math.floor(Math.random() * range);

  if (spec.type === "price") {
    return {
      _id: UUID.v4().toString(),
      title: `${spec.title} ${spec.price.min + hit} %`,
      description: `When purchase ${spec.price.base} B.`,
      created: (new Date()).toString(),
      expired: "18:30 hr",
      specID: spec._id || "WOW",
      type: spec.type,
      vender: spec.vender,
    };
  } else {
    return {
      _id: UUID.v4().toString(),
      title: spec.title,
      description: spec.description,
      created: (new Date()).toString(),
      expired: "18:30 hr",
      specID: spec._id || "WOW",
      type: type || spec.type,
      vender: spec.vender,
      product: {
        id: spec.product.id,
        basePrice: spec.price.base,
        salePrice: spec.price.min + hit,
      },
    };
  }
};

const CouponSpecData = {
  resolve: async (length = 5) => {
    const list = await CouponSpecData.getAll();
    const indexes = [];

    while (indexes.length < length) {
      const i = Math.floor(Math.random() * list.length);

      if (indexes.indexOf(i) === -1) {
        indexes.push(i);
      }
    }

    return indexes.map((i) => transform(list[i]));
  },
  getAll: async () => {
    const collection = await Collection();
    const snapshot = await collection.get();
    const list = [];
    snapshot.forEach((doc) => list.push(doc.data()));

    return list;
  },
  set: async (couponSpec: ICouponSpec) => {
    try {
      const collection = await Collection();
      await collection.doc(couponSpec._id).set(couponSpec);

      return {
        success: true,
      };
    } catch (error) {
      return {
        success: false,
        error,
      };
    }
  },
};

export const gen = () => {
  // CouponSpecData.set({
  //   _id: UUID.v4().toString(),
  //   type: "product",
  //   vender: "TOPS",
  //   title: "Tops Drinking Water",
  //   description: "6 x 1500ml.",
  //   product: {
  //     id: "8853474057887",
  //   },
  //   lifesec: 60 * 60 * 2,
  //   price: {
  //     base: 55,
  //     max: 35,
  //     min: 42,
  //   },
  // });

  CouponSpecData.set({
    _id: UUID.v4().toString(),
    type: "product",
    vender: "PowerBuy",
    title: "SAMSUNG Galaxy S10+",
    description: "512 GB, Ceramic White",
    product: {
      id: UUID.v4().toString(),
    },
    lifesec: 60 * 60 * 2,
    price: {
      base: 44900, // จากเดิม
      max: 41900, // ลดแล้ว
      min: 38900, // ลดอีก
    },
  });
};

export default CouponSpecData;
