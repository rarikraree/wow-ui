import { createAppContainer, createSwitchNavigator } from "react-navigation";

import HomeScreen from "../screens/HomeScreen";
import CouponsStack from "../screens/Coupons/CouponsStack";
import FollowStack from "../screens/Follow/FollowStack";
import BuyStack from "../screens/Buy/BuyStack";

import Test from "../screens/Coupons/Tabs/WoWCoupons";

export default createAppContainer(
  createSwitchNavigator(
    {
      HOME: HomeScreen,
      COUPONS: CouponsStack,
      FOLLOW: FollowStack,
      BUY: BuyStack,
      TEST: Test,
    },
    {
      initialRouteName: "HOME",
    },
  )
);
