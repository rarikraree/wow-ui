import React from "react";
import { TouchableOpacity, View, StyleSheet } from "react-native";
import { Button, Card, Title, Paragraph } from "react-native-paper";
import { ICoupon } from "../data/Coupons/ICoupon";

interface Props {
  coupon: ICoupon;
  dismiss: any;
  take: any;
}

const PriceCoupon: React.FC<Props> = ({ coupon, dismiss, take }) => (
  <View style={styles.wrapper}>
    <Card>
      <Card.Content>
        <Title style={styles.title}>{coupon.title}</Title>
        <Paragraph>{`${coupon.description}`}</Paragraph>
      </Card.Content>
      <Card.Actions>
        <Title style={styles.vender}>{`${coupon.vender}`}</Title>
        <View style={{ flex: 1, flexDirection: "row" }} />
        <Button onPress={dismiss}>Dismiss</Button>
        <Button onPress={take}>Take</Button>
      </Card.Actions>
    </Card>
  </View>
);

const styles = StyleSheet.create({
  wrapper: {
    padding: 6,
  },
  container: {
    backgroundColor: "#fff",
    flexDirection: "row",
  },
  main: {
    flex: 1,
  },
  side: {
    width: "30%",
  },
  label: {
    fontWeight: "bold"
  },
  basePrice: {
    textDecorationLine: "line-through",
    textDecorationStyle: "solid",
    fontSize: 20,
  },
  salePrice: {
    fontSize: 35,
    fontWeight: "bold",
    color: "red",
  },
  title: {
    fontWeight: "bold",
  },
  vender: {
    fontWeight: "bold",
    fontSize: 15,
    marginLeft: 10,
  },
});

export default PriceCoupon;
