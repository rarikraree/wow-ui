import React from "react";

import { ICoupon } from "../data/Coupons/ICoupon";
import PriceCoupon from "./PriceCoupon";
import ProductCoupon from "./ProductCoupon";

interface Props {
  coupon: ICoupon;
  dismiss: any;
  take: any;
}

const Coupon: React.FC<Props> = ({ coupon, dismiss, take }) => {
  const Component = coupon.type === "price" ? PriceCoupon : ProductCoupon;

  return (
    <Component
      coupon={coupon}
      dismiss={dismiss}
      take={take}
    />
  );
};

export default Coupon;
