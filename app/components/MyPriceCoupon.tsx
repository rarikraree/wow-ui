import React from "react";
import { View, StyleSheet } from "react-native";
import { Button, Card, Title, Paragraph } from "react-native-paper";
import { ICoupon } from "../data/Coupons/ICoupon";

interface Props {
  coupon: ICoupon;
  dismiss: any;
}

const MyPriceCoupon: React.FC<Props> = ({ coupon, dismiss }) => (
  <View style={styles.wrapper}>
    <Card>
      <Card.Content>
        <Title style={styles.title}>{coupon.title}</Title>
        <Paragraph>{`(${coupon.description})`}</Paragraph>
        <Paragraph>{`Exp: ${coupon.expired}`}</Paragraph>
      </Card.Content>
      <Card.Actions>
        <Title style={styles.vender}>{`${coupon.vender}`}</Title>
        <View style={{ flex: 1, flexDirection: "row" }} />
        <Button onPress={dismiss}>Dismiss</Button>
      </Card.Actions>
    </Card>
  </View>
);

const styles = StyleSheet.create({
  wrapper: {
    padding: 6,
  },
  title: {
    fontWeight: "bold",
  },
  vender: {
    fontWeight: "bold",
    fontSize: 15,
    marginLeft: 10,
  },
});

export default MyPriceCoupon;
