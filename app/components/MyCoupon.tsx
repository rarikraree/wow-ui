import React from "react";

import MyPriceCoupon from "./MyPriceCoupon";
import MyProductCoupon from "./MyProductCoupon";
import MyWoWCoupon from "./MyWoWCoupon";

interface Props {
  coupon: any;
  dismiss: any;
}

const MyCounpon: React.FC<Props> = ({ coupon, dismiss }) => {
  let Component = coupon.type === "price" ? MyPriceCoupon : coupon.type === "product" ? MyProductCoupon : MyWoWCoupon;

  return (
    <Component
      coupon={coupon}
      dismiss={dismiss}
    />
  );
};

export default MyCounpon;
