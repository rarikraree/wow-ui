import React from "react";
import { StyleSheet, View, Text } from "react-native";
import { Button, Card, Title, Paragraph, Avatar } from "react-native-paper";
import { ICoupon } from "../data/Coupons/ICoupon";

interface Props {
  coupon: ICoupon;
  dismiss: any;
  take: any;
}

const WoWCoupon: React.FC<Props> = ({ coupon, dismiss, take }) => (
  <View style={styles.wrapper}>
    <Card>
      <Card.Title
        title="WoW Price"
        left={(props) => (
          <Avatar.Image
            size={40}
            source={require("../../assets/logo.png")}
            style={styles.avatar}
          />
        )} />
      <Card.Content>
        <Title style={styles.title}>{coupon.title}</Title>
        <View style={styles.container}>
          <View style={styles.main}>
            <Paragraph>{`(${coupon.description})`}</Paragraph>
            <Paragraph>{`Exp: ${coupon.expired}`}</Paragraph>
          </View>
          <View style={styles.side}>
            <Text style={styles.label}>Special Price</Text>
            <Text style={styles.salePrice}>{`${coupon.product.salePrice}`}</Text>
            <Text style={styles.basePrice}>{coupon.product.basePrice}</Text>
          </View>
        </View>
      </Card.Content>
      <Card.Actions>
        <Text style={styles.vender}>{`${coupon.vender}`}</Text>
        <View style={{ flex: 1, flexDirection: "row" }} />
        <Button onPress={dismiss}>Dismiss</Button>
        <Button onPress={take}>Take</Button>
      </Card.Actions>
    </Card>
  </View>
);

const styles = StyleSheet.create({
  wrapper: {
    padding: 6,
  },
  container: {
    backgroundColor: "#fff",
    flexDirection: "row",
  },
  main: {
    flex: 1,
  },
  side: {
    width: "30%",
  },
  label: {
    fontWeight: "bold"
  },
  basePrice: {
    textDecorationLine: "line-through",
    textDecorationStyle: "solid",
    fontSize: 20,
  },
  salePrice: {
    fontSize: 32,
    fontWeight: "bold",
    color: "red",
  },
  title: {
    fontWeight: "bold",
  },
  vender: {
    fontWeight: "bold",
    marginLeft: 10,
  },
  avatar: {
    marginLeft: 10,
    borderWidth: 1,
  },
});

export default WoWCoupon;
