import React from "react";
import { StyleSheet, View, Text } from "react-native";
import { Button, Card, Title, Paragraph } from "react-native-paper";

interface Props {
  buy: any;
  dismiss: any;
}

const Buy: React.FC<Props> = ({ buy, dismiss }) => (
  <View style={styles.wrapper}>
    <Card>
      <Card.Content>
        <Title style={styles.title}>
          {"DROP n GO"}
        </Title>
        <View style={styles.container}>
          <View style={styles.main}>
            <Paragraph style={styles.status}>{`Status: ${buy.status}`}</Paragraph>
            <Paragraph style={styles.count}>{`Items Count: ${buy.itemCount == 0 ? "-" : buy.itemCount}`}</Paragraph>
          </View>
          <View style={styles.side}>
            <Text style={styles.label}>Total</Text>
            <Text style={styles.total}>{buy.orderTotal == 0 ? "-" : buy.orderTotal}</Text>
          </View>
        </View>
      </Card.Content>
      <Card.Actions>
        <Text style={styles.vender}>TOPS</Text>
        <View style={{ flex: 1, flexDirection: "row" }} />
        <Button onPress={dismiss}>Dismiss</Button>
      </Card.Actions>
    </Card>
  </View>
);

const styles = StyleSheet.create({
  wrapper: {
    padding: 6,
  },
  container: {
    backgroundColor: "#fff",
    flexDirection: "row",
  },
  main: {
    flex: 1,
  },
  side: {
    width: "30%",
  },
  status: {
    fontSize: 16,
    fontWeight: "bold",
  },
  count: {
    fontSize: 14,
  },
  label: {
    fontWeight: "bold"
  },
  total: {
    fontSize: 32,
    fontWeight: "bold",
    color: "red",
  },
  title: {
    fontWeight: "bold",
  },
  vender: {
    fontWeight: "bold",
    marginLeft: 10,
  },
});

export default Buy;
