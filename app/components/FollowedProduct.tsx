import React from "react";
import { View, StyleSheet } from "react-native";
import { Button, Card, Title, Paragraph } from "react-native-paper";

interface Props {
  name: string;
  vender: string;
  location: string;
}

const FollowedProduct: React.FC<Props> = ({ name, vender, location }) => (
  <View style={styles.wrapper}>
    <Card>
      <Card.Content>
        <Title style={styles.title}>{name}</Title>
        <Paragraph>{`Location: ${location}`}</Paragraph>
      </Card.Content>
      <Card.Actions>
        <Title style={styles.vender}>{vender}</Title>
        <View style={{ flex: 1, flexDirection: "row" }} />
        <Button>Unfollow</Button>
      </Card.Actions>
    </Card>
  </View>
);

const styles = StyleSheet.create({
  wrapper: {
    padding: 6,
  },
  container: {
    backgroundColor: "#fff",
    flexDirection: "row",
  },
  main: {
    flex: 1,
  },
  side: {
    width: "30%",
  },
  label: {
    fontWeight: "bold"
  },
  basePrice: {
    textDecorationLine: "line-through",
    textDecorationStyle: "solid",
    fontSize: 20,
  },
  salePrice: {
    fontSize: 35,
    fontWeight: "bold",
    color: "red",
  },
  title: {
    fontWeight: "bold",
  },
  vender: {
    fontWeight: "bold",
    fontSize: 15,
    marginLeft: 10,
  },
});

export default FollowedProduct;
