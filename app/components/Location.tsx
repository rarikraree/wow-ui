import React from "react";
import { StyleSheet, View, Text } from "react-native";

interface Props {
  location: string;
}

const Location: React.FC<Props> = ({ location }) => (
  <View style={styles.wrapper}>
    <View style={styles.card}>
      <Text style={styles.title}>Location: {location}</Text>
    </View>
  </View>
);

const styles = StyleSheet.create({
  wrapper: {
    padding: 6,
  },
  title: {
    fontWeight: "bold",
    fontSize: 16,
    textAlign: "center",
    padding: 8,
  },
  card: {
    backgroundColor: "#fff",
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "#ccc"
  }
});

export default Location;
