import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { NavigationContainerProps } from "react-navigation";
import { Avatar } from 'react-native-paper';

interface Props extends NavigationContainerProps {
}

const HomeScreen: React.FC<Props> = ({ navigation }) => (
  <View style={styles.container}>
    <Avatar.Image
      size={300}
      source={require("../../assets/logo.png")}
      style={styles.avatar}
    />
    <Text>v.0.0.1</Text>
    <View style={styles.navigation}>
      <View style={styles.row1}>
        <TouchableOpacity
          style={styles.box1}
          onPress={() => navigation.navigate("COUPONS")}
        >
          <Text style={styles.text}>COUPONS</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.box2}
          onPress={() => navigation.navigate("FOLLOW")}
        >
          <Text style={styles.text}>FOLLOW</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.row2}>
        <TouchableOpacity
          style={styles.box1}
          onPress={() => navigation.navigate("BUY")}
        >
          <Text style={styles.text}>BUY</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.box2}
        >
          <Text style={styles.text}>DELIVERY</Text>
        </TouchableOpacity>
      </View>
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    backgroundColor: "#fff",
    flex: 1,
    justifyContent: "center",
  },
  navigation: {
    bottom: 0,
    position: "absolute",
    width: "100%",
  },
  row1: {
    borderColor: "#999",
    borderTopWidth: 1,
    flexDirection: "row",
    width: "100%",
  },
  row2: {
    borderColor: "#999",
    borderTopWidth: 1,
    borderBottomWidth: 1,
    flexDirection: "row",
    width: "100%",
  },
  box1: {
    alignItems: "center",
    borderColor: "#999",
    borderLeftWidth: 1,
    flex: 1,
    paddingTop: 24,
    paddingBottom: 24,
  },
  box2: {
    alignItems: "center",
    borderColor: "#999",
    borderLeftWidth: 1,
    borderRightWidth: 1,
    flex: 1,
    paddingTop: 24,
    paddingBottom: 24,
  },
  text: {
    fontSize: 20,
    fontWeight: "bold",
  },
  avatar: {
    borderWidth: 1,
  }
});

export default HomeScreen;
