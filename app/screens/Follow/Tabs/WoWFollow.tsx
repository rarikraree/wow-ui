import React from "react";
import { Button, StyleSheet, View } from "react-native";
import { Text } from "react-native-paper";
import Constants from "expo-constants";
import * as Permissions from "expo-permissions";
import { BarCodeScanner } from "expo-barcode-scanner";

interface Props {
}

interface State {
  hasCameraPermission?: boolean;
  scanned: boolean;
}

export default class WoWCoupons extends React.Component<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      hasCameraPermission: undefined,
      scanned: false,
    };
  }

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);

    this.setState({
      hasCameraPermission: status === "granted",
    });
  }

  _handleBarCodeScanned = ({ type, data }) => {
    this.setState({ scanned: true });
    alert(`Bar code with type ${type} and data ${data} has been scanned!`);
  };

  render() {
    const { hasCameraPermission, scanned } = this.state;

    if (hasCameraPermission === undefined) {
      return (<Text>Requesting for camera permission</Text>);
    } else if (hasCameraPermission === false) {
      return (<Text>No access to camera</Text>);
    } else {
      return (
        <View style={styles.container}>
          <BarCodeScanner
            onBarCodeScanned={scanned ? undefined : this._handleBarCodeScanned}
            style={StyleSheet.absoluteFillObject}
          />

          {scanned && (
            <Button title={"Tap to Scan Again"} onPress={() => this.setState({ scanned: false })} />
          )}
        </View>
      );
    }
  };
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    backgroundColor: "#000",
    flex: 1,
    justifyContent: "center",
  },
});
