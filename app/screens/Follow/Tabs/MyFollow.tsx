import React from "react";
import { StyleSheet, Text, SafeAreaView, ScrollView, View } from 'react-native';
import { BottomNavigation } from "react-native-paper";

import MyCouponsData from "../../../data/Coupons/MyCouponsData";
import FollowedProduct from "../../../components/FollowedProduct";

interface Props {
}

interface State {
}

class MyFollow extends React.Component<Props, State> {
  async componentDidMount() {
    const list = await MyCouponsData.getAll();
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView style={styles.scrollView}>
          <FollowedProduct
            name="APPLE iPhone11"
            vender="PowerBuy"
            location="Central World"
          />
          <FollowedProduct
            name="Nike Epic React Flyknit 2"
            vender="Supersports"
            location="Central Silom Complex"
          />
          <FollowedProduct
            name="Nestles Pure Life Water"
            vender="Tops"
            location="Silom Tower"
          />
          <FollowedProduct
            name="Canon Pixma G1010 Inkjet Printer"
            vender="OfficeMate"
            location="Central World"
          />
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ccc",
  },
  scrollView: {
    backgroundColor: "#ccc",
    marginHorizontal: 0,
  },
});

export default MyFollow;
