import { createStackNavigator } from "react-navigation-stack";

import FollowScreen from "./FollowScreen";

const FollowStack = createStackNavigator({
  Follow: {
    screen: FollowScreen
  },
})

export default FollowStack;
