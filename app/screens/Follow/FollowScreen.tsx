import React from "react";
import { StyleSheet } from "react-native";
import { BottomNavigation } from "react-native-paper";

import withNavigationBar from "../../components/withNavigationBar";

import MyFollow from "./Tabs/MyFollow";
import WoWFollow from "./Tabs/WoWFollow";

interface Props {
}

interface State {
  index: number;
  routes: any[];
}

class CouponsScreen extends React.Component<Props, State> {
  static navigationOptions = withNavigationBar({ title: "Follow" });

  constructor(props) {
    super(props);

    this.state = {
      index: 0,
      routes: [
        { key: "MYFOLLOW", title: "My Follow", icon: "heart" },
        { key: "WOWFOLLOW", title: "Product", icon: "barcode-scan" },
      ],
    };
  }

  _handleIndexChange = index => this.setState({ index });

  _renderScene = BottomNavigation.SceneMap({
    MYFOLLOW: MyFollow,
    WOWFOLLOW: WoWFollow,
  } as any);

  render() {
    return (
      <BottomNavigation
        navigationState={this.state}
        onIndexChange={this._handleIndexChange}
        renderScene={this._renderScene}
      />
    );
  };
}

const styles = StyleSheet.create({
  container: {
    // backgroundColor: "#fff",
  },
});

export default CouponsScreen;
