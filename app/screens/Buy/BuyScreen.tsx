import React from "react";
import { BottomNavigation } from "react-native-paper";

import withNavigationBar from "../../components/withNavigationBar";

import DropAndGo from "./Tabs/DropAndGo";
import MyBuy from "./Tabs/MyBuy";

interface Props {
}

interface State {
  index: number;
  routes: any[];
}

class BuyScreen extends React.Component<Props, State> {
  static navigationOptions = withNavigationBar({ title: "Buy" });

  constructor(props) {
    super(props);

    this.state = {
      index: 0,
      routes: [
        { key: "MYBUY", title: "My Buy", icon: "cart" },
        { key: "DROPANDGO", title: "Drop n Go", icon: "barcode-scan" },
      ],
    };
  }

  _handleIndexChange = index => this.setState({ index });

  _renderScene = BottomNavigation.SceneMap({
    DROPANDGO: DropAndGo,
    MYBUY: MyBuy,
  } as any);

  render() {
    return (
      <BottomNavigation
        navigationState={this.state}
        onIndexChange={this._handleIndexChange}
        renderScene={this._renderScene}
      />
    );
  };
}

export default BuyScreen;
