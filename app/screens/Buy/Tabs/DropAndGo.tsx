import React from "react";
import { Button, StyleSheet, View } from "react-native";
import { Text } from "react-native-paper";
import * as Permissions from "expo-permissions";
import { BarCodeScanner } from "expo-barcode-scanner";

import { ICoupon } from "../../../data/Coupons/ICoupon";
import { ICouponSpec } from "../../../data/Coupons/ICouponSpec";
import BuyData from "../../../data/Buy/BuyData";

interface Props {
}

interface State {
  hasCameraPermission?: boolean;
  scanned: boolean;
  ref?: string;
}

export default class WoWCoupons extends React.Component<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      hasCameraPermission: undefined,
      scanned: false,
    };
  }

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);

    this.setState({
      hasCameraPermission: status === "granted",
    });
  }

  _handleBarCodeScanned = async ({ type, data }) => {
    try {
      this.setState({ scanned: true, ref: data });
      alert("You have DROPPED and you may GO!!");

      await BuyData.setDropAndGo(data);

      setTimeout(() => {
        this.setState({ scanned: false, ref: null });
      }, 3000)
    } catch (error) {
      alert(error);
    }
  };

  render() {
    const { hasCameraPermission, scanned } = this.state;

    if (hasCameraPermission === undefined) {
      return (<Text>Requesting for camera permission</Text>);
    } else if (hasCameraPermission === false) {
      return (<Text>No access to camera</Text>);
    } else {
      if (this.state.ref) {
        return (
          <View style={styles.load}>
            <Text>Loading...</Text>
            <Text>REF: {this.state.ref}</Text>
          </View>
        );
      } else {
        return (
          <View style={styles.container}>
            <BarCodeScanner
              onBarCodeScanned={scanned ? undefined : this._handleBarCodeScanned}
              style={StyleSheet.absoluteFillObject}
            />

            {scanned && (
              <Button title={"Tap to Scan Again"} onPress={() => this.setState({ scanned: false })} />
            )}
          </View>
        );
      }
    }
  };
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    backgroundColor: "#000",
    flex: 1,
    justifyContent: "center",
  },
  load: {
    alignItems: "center",
    backgroundColor: "#fff",
    flex: 1,
    justifyContent: "center",
  },
});
