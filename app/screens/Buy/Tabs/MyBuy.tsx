import React from "react";
import { StyleSheet, SafeAreaView, ScrollView } from 'react-native';

import BuyData from "../../../data/Buy/BuyData";
import { IBuy } from "../../../data/Buy/IBuy";

import Buy from "../../../components/Buy";

interface Props {
}

interface State {
  isLoading: boolean;
  buys: IBuy[];
}

class MyCounpons extends React.Component<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      buys: [],
    };

    console.ignoredYellowBox = [
      "Setting a timer",
    ];
  }

  async _refresh() {
    if (!this.state.isLoading) {
      this.setState({
        isLoading: true,
      });

      const buys = await BuyData.getAll();

      this.setState({
        isLoading: false,
        buys,
      });
    }
  }

  async componentDidMount() {
    this._refresh();
  }

  async _handleDismiss(index: number) {
    const { ref } = this.state.buys[index];

    await BuyData.delete(ref);
    await this._refresh();
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView
          style={styles.scrollView}
          onScrollBeginDrag={() => this._refresh()}
          onScrollEndDrag={() => this._refresh()}
        >
          {
            this.state.buys.map((buy, index) => (
              <Buy
                key={index}
                buy={buy}
                dismiss={() => this._handleDismiss(index)}
              />
            ))
          }
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ccc",
  },
  scrollView: {
    backgroundColor: "#ccc",
    marginHorizontal: 0,
  },
});

export default MyCounpons;
