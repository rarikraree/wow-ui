import { createStackNavigator } from "react-navigation-stack";

import BuyScreen from "./BuyScreen";

const BuyStack = createStackNavigator({
  Coupons: {
    screen: BuyScreen
  },
})

export default BuyStack;
