import React from "react";
import { StyleSheet, SafeAreaView, ScrollView } from 'react-native';

import MyCouponsData from "../../../data/Coupons/MyCouponsData";

import MyCoupon from "../../../components/MyCoupon";

interface Props {
}

interface State {
  coupons: any[];
  isLoading: boolean;
}

class MyCounpons extends React.Component<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      coupons: [],
      isLoading: false,
    }

    console.ignoredYellowBox = [
      "Setting a timer",
    ];
  }

  async _refresh() {
    if (!this.state.isLoading) {
      this.setState({
        isLoading: true,
      });

      const coupons = await MyCouponsData.getAll();

      this.setState({
        isLoading: false,
        coupons,
      });
    }
  }

  async _handleDelete(index) {
    const deleted = this.state.coupons[index];
    const coupons = this.state.coupons.filter((v, i) => i !== index);

    this.setState({
      coupons,
    });

    await MyCouponsData.delete(deleted._id);
  }

  async componentDidMount() {
    this._refresh();
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView
          style={styles.scrollView}
          onScrollBeginDrag={() => this._refresh()}
          onScrollEndDrag={() => this._refresh()}
        >
          {
            this.state.coupons.map((coupon, index) => (
              <MyCoupon
                key={index}
                coupon={coupon}
                dismiss={() => this._handleDelete(index)}
              />
            ))
          }
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ccc",
  },
  scrollView: {
    backgroundColor: "#ccc",
    marginHorizontal: 0,
  },
});

export default MyCounpons;
