import React from "react";
import { StyleSheet, SafeAreaView, ScrollView } from 'react-native';

import Coupon from "../../../components/Coupon";
import Location from "../../../components/Location";

import CouponSpecData from "../../../data/Coupons/CouponSpecData";
import MyCouponsData from "../../../data/Coupons/MyCouponsData";

interface Props {
}

interface State {
  coupons: any[];
}

class LocationCounpons extends React.Component<Props, State> {
  constructor(props) {
    super(props);

    console.ignoredYellowBox = [
      "Setting a timer",
    ];

    this.state = {
      coupons: [],
    }
  }

  async _getCoupons() {
    const coupons = await CouponSpecData.resolve(7);

    this.setState({
      coupons,
    });
  }

  async _handleDismiss(index) {
    const coupons = this.state.coupons.filter((v, i) => i !== index);

    this.setState({
      coupons,
    });
  }

  async _handleTake(index) {
    const taken = this.state.coupons[index];
    const coupons = this.state.coupons.filter((v, i) => i !== index);

    MyCouponsData.set(taken);

    this.setState({
      coupons,
    });
  }

  render() {
    if (this.state.coupons.length === 0) {
      this._getCoupons();
    }

    return (
      <SafeAreaView style={styles.container}>
        <Location location="Central World" />
        <ScrollView style={styles.scrollView}>
          {this.state.coupons.map((coupon, index) => (
            <Coupon
              key={index}
              dismiss={() => this._handleDismiss(index)}
              take={() => this._handleTake(index)}
              coupon={coupon}
            />
          ))}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ccc",
  },
  scrollView: {
    backgroundColor: "#ccc",
    marginHorizontal: 0,
  },
});

export default LocationCounpons;
