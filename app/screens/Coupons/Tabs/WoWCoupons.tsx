import React from "react";
import { Button, StyleSheet, View } from "react-native";
import { Text } from "react-native-paper";
import * as Permissions from "expo-permissions";
import { BarCodeScanner } from "expo-barcode-scanner";

import { ICoupon } from "../../../data/Coupons/ICoupon";
import { ICouponSpec } from "../../../data/Coupons/ICouponSpec";
import MyCouponsData from "../../../data/Coupons/MyCouponsData";
import { transform } from "../../../data/Coupons/CouponSpecData";
import WoWCoupon from "../../../components/WoWCoupon";

interface Props {
}

interface State {
  hasCameraPermission?: boolean;
  scanned: boolean;
  coupon?: ICoupon;
}

export default class WoWCoupons extends React.Component<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      hasCameraPermission: undefined,
      scanned: false,
    };
  }

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);

    this.setState({
      hasCameraPermission: status === "granted",
    });
  }

  _handleBarCodeScanned = ({ type, data }) => {
    try {
      const AingSpec: ICouponSpec = JSON.parse(data);
      const { base, max, min } = AingSpec.price;
      const l = [base, max, min].sort();
      const TrueSpec: ICouponSpec = {
        ...AingSpec,
        price: {
          base: l[2],
          max: l[1],
          min: l[0],
        },
      };

      this.setState({ scanned: true, coupon: transform(TrueSpec, "wow") });
    } catch (error) {
      alert(error);
    }
  };

  _dismiss() {
    this.setState({
      coupon: undefined,
      scanned: false,
    });
  }

  async _take() {
    await MyCouponsData.set(this.state.coupon);

    this.setState({
      coupon: undefined,
      scanned: false,
    });
  }

  render() {
    const { hasCameraPermission, scanned } = this.state;

    if (hasCameraPermission === undefined) {
      return (<Text>Requesting for camera permission</Text>);
    } else if (hasCameraPermission === false) {
      return (<Text>No access to camera</Text>);
    } else {
      if (this.state.coupon) {
        return (
          <View>
            <WoWCoupon
              coupon={this.state.coupon}
              dismiss={() => this._dismiss()}
              take={() => this._take()}
            />
          </View>
        );
      } else {
        return (
          <View style={styles.container}>
            <BarCodeScanner
              onBarCodeScanned={scanned ? undefined : this._handleBarCodeScanned}
              style={StyleSheet.absoluteFillObject}
            />

            {scanned && (
              <Button title={"Tap to Scan Again"} onPress={() => this.setState({ scanned: false })} />
            )}
          </View>
        );
      }
    }
  };
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    backgroundColor: "#000",
    flex: 1,
    justifyContent: "center",
  },
  coupon: {
    backgroundColor: "#fff",
    flex: 1,
  },
});
