import React from "react";
import { BottomNavigation } from "react-native-paper";

import withNavigationBar from "../../components/withNavigationBar";

import { gen } from "../../data/Coupons/CouponSpecData";

import MyCoupons from "./Tabs/MyCoupons";
import LocationCoupons from "./Tabs/LocationCoupons";
import WoWCoupons from "./Tabs/WoWCoupons";

interface Props {
}

interface State {
  index: number;
  routes: any[];
}

class CouponsScreen extends React.Component<Props, State> {
  static navigationOptions = withNavigationBar({ title: "Coupons" });

  constructor(props) {
    super(props);

    gen();
    this.state = {
      index: 0,
      routes: [
        { key: "MYCOUPONS", title: "My Coupons", icon: "cards" },
        { key: "LOCATIONCOUPONS", title: "Location", icon: "map-marker" },
        { key: "WOWCOUPONS", title: "WoW Price", icon: "barcode-scan" },
      ],
    };
  }

  _handleIndexChange = index => this.setState({ index });

  _renderScene = BottomNavigation.SceneMap({
    MYCOUPONS: MyCoupons,
    LOCATIONCOUPONS: LocationCoupons,
    WOWCOUPONS: WoWCoupons,
  } as any);

  render() {
    return (
      <BottomNavigation
        navigationState={this.state}
        onIndexChange={this._handleIndexChange}
        renderScene={this._renderScene}
      />
    );
  };
}

export default CouponsScreen;
