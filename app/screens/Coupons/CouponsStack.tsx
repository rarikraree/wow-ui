import { createStackNavigator } from "react-navigation-stack";

import CouponsScreen from "./CouponsScreen";

const CouponsStack = createStackNavigator({
  Coupons: {
    screen: CouponsScreen
  },
})

export default CouponsStack;
