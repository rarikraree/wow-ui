import React from "react";
import { AppLoading } from "expo";
import { DefaultTheme, Provider as PaperProvider } from "react-native-paper";

import { FirebaseClient } from "./app/data/common/FirebaseClient";
import AppNavigation from "./app/navigation/AppNavigation";

interface Props {
}

interface State {
  isLoadingComplete: boolean;
}

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: "#d22",
    accent: "#ddd",
  },
};

export default class App extends React.Component<Props, State> {
  constructor(props) {
    super(props);

    console.ignoredYellowBox = [
      "Setting a timer",
    ];

    this.state = {
      isLoadingComplete: false,
    };
  }

  async componentDidMount() {
    await FirebaseClient.connect();

    this.setState({
      isLoadingComplete: true,
    });
  }

  render() {
    if (!this.state.isLoadingComplete) {
      return (
        <AppLoading />
      );
    } else {
      return (
        <PaperProvider theme={theme}>
          <AppNavigation />
        </PaperProvider>
      );
    }
  };
}
